# UwUAPIV2

Just like the first version but in TypeScripts, Node.js and Express with some extra features

# Installation
Pretty simple:
* Install the modules: `npm install`
* Build: `npm run build`
* Now run: node .

# Usage
You can make a token from genToken.js in the dist folder after building the files